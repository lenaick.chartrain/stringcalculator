<?php


namespace App\Tests;


use App\StringCalculator;
use PHPUnit\Framework\TestCase;

class TestStringCalculator extends TestCase
{
    public function testGivenAStringWhenStringIsEmptyThenIshouldHave0()
    {
        $stringCalculator = new StringCalculator();
        $result = $stringCalculator->add("");

        $this->assertEquals(0, $result);

    }

    public function testGivenAStringWhenAStringContentOneNumberThenIShouldReturnNumber()
    {
        $stringCalculator = new StringCalculator();
        $result = $stringCalculator->add("0");

        $this->assertEquals(0, $result);
    }

    public function testGivenAStringWhenAStringContent2NumbersSeparateWithCommaThenIShouldReturnAdd()
    {
        $stringCalculator = new StringCalculator();
        $result = $stringCalculator->add("0,1");

        $this->assertEquals(1, $result);
    }

    public function testGivenAStringWhenAStringContent2NumbersWithDotSeparateWithCommaThenIShouldReturnAdd()
    {
        $stringCalculator = new StringCalculator();
        $result = $stringCalculator->add("0.2,1.3");

        $this->assertEquals(1.5, $result);
    }

    public function testGivenAStringWhenAStringContentXNumbersSeparateWithCommaThenIShouldReturnAdd()
    {
        $stringCalculator = new StringCalculator();
        $result = $stringCalculator->add("0,1,2,3,4,5");

        $this->assertEquals(15, $result);
    }

    public function testGivenAStringWhenAStringContentXNumbersSeparateWithNewLineAndCommaThenIShouldReturnAdd()
    {
        $stringCalculator = new StringCalculator();
        $result = $stringCalculator->add("0\n1,2,3,4,5,6");

        $this->assertEquals(21, $result);
    }

    public function testGivenAStringWhenTwoDelimitersAreCoteCoteThenIShouldHaveAnException()
    {
        $stringCalculator = new StringCalculator();
        $this->expectException(\LogicException::class);
        $result = $stringCalculator->add("0,\n1,2,3,4,5,6,7");
    }

    public function testGivenAStringWhenLastCharacterIsCommaThenIShouldHaveAnException()
    {
        $stringCalculator = new StringCalculator();
        $this->expectException(\LogicException::class);
        $result = $stringCalculator->add("0,1,2,3,");
    }

    public function testGivenAStringWhenCustomSeparatorThenIShouldAddNumbers()
    {
        $stringCalculator = new StringCalculator();
        $result = $stringCalculator->add("//;\n1;2");
        $this->assertEquals(3, $result);

        $result = $stringCalculator->add("//|\n1|2|3");
        $this->assertEquals(6, $result);

        $result = $stringCalculator->add("//sep\n2sep3");
        $this->assertEquals(5, $result);

        $this->expectException(\LogicException::class);
        $result = $stringCalculator->add("//|\n1|2,3");
    }

    public function testGivenAStringWhenNegativeNumbersThenIShouldHaveAnException()
    {
        $stringCalculator = new StringCalculator();
        $this->expectException(\LogicException::class);
        $result = $stringCalculator->add("-1,2");

        $this->expectException(\LogicException::class);
        $result = $stringCalculator->add("2,-4,-5");
    }
}