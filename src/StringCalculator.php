<?php


namespace App;


class StringCalculator
{
    public function add(string $stringCalculator): float
    {
        if (empty($stringCalculator)) return 0;

        if($position = strpos($stringCalculator, ",\n") !== false) {
            $message = "Number expected but \\n found at position {$position}.";
            throw new \LogicException($message);
        }

        if (substr($stringCalculator, -1) == ",") {
            $message = "Number expected but EOF found.";
            throw new \LogicException($message);
        }

        $delimiter = $this->getSeparator($stringCalculator);

        $numbers = $this->getNumbers($stringCalculator, $delimiter);
        $result = 0;
        $message = "";

        foreach ($numbers as $number) {
            $message .= (!empty($message)) ? "," : "";
            if ($number < 0) {
                $message .= $number;
            }
            $result += floatval($number);
        }

        if (!empty($message)) {
            $message = "Negative not allowed : " . $message;
            throw new \LogicException($message);
        }

        return $result;
    }

    private function getNumbers(string $stringCalculator, string $delimiter): array
    {
        $string = str_replace(",", $delimiter, $stringCalculator);

        if (strpos($string, $delimiter) !== false) {
            return explode($delimiter, $string);
        } else {
            return [$stringCalculator];
        }
    }

    private function getSeparator(string $stringCalculator): string
    {
        if (strpos($stringCalculator, "//") === 0) {
            $delimiter = substr($stringCalculator, 2, (strpos($stringCalculator, "\n")-2));
            $this->isMultipleSeparators($stringCalculator, $delimiter);
            return $delimiter;
        }

        return "\n";
    }

    private function isMultipleSeparators($stringCalculator, $delimiter): void
    {
        if ($position = strpos($stringCalculator, ",") !== false) {
            $message = "$delimiter expected but ',' found at position $position.";
            throw new \LogicException($message);
        }
    }
}